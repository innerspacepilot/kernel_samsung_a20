#!/bin/bash
#
# Custom build script for Eureka kernels by Chatur27 and Gabriel260 @Github - 2020
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Set default directories
ROOT_DIR=$(pwd)
# OUT_DIR=$ROOT_DIR/out
KERNEL_DIR=$ROOT_DIR

# Set default kernel variables
PROJECT_NAME="OneUI Kernel"
CORES=$(nproc --all)
ZIPNAME=OneUI
GCC_ARM64_FILE=aarch64-linux-gnu-
DEFCONFIG=oneui-a20_defconfig
TOOLCHAIN_DIR=../toolchain/bin
# Export commands
export KBUILD_BUILD_USER=arpio
export KBUILD_BUILD_HOST=workstation
export ANDROID_MAJOR_VERSION=q
export VERSION=OneUI
export ARCH=arm64
export CROSS_COMPILE=$TOOLCHAIN_DIR"/"$GCC_ARM64_FILE

# Get date and time
DATE=$(date +"%m-%d-%y")
BUILD_START=$(date +"%s")

################### Executable functions #######################
CLEAN_SOURCE()
{
	echo "*****************************************************"
	echo " "
	echo "              Checking build directory"
	echo " "
	echo "*****************************************************"
	if [ ! -d "$(dirname $(dirname $(realpath $0)) )/A20_KERNEL_BUILD" ]; then
        echo "Build directory does not exist, starting build process..."
        sleep 2
        ENTER_VERSION
        clear
        SELECT_CONFIG
		clear
        COMMON_STEPS
    else
        read -r -p "Removing build directory: $(dirname $(dirname $(realpath $0)) )/A20_KERNEL_BUILD? [Y/n] " input
        
        case $input in
            [yY][eE][sS]|[yY])
        rm -r $(dirname $(dirname $(realpath $0)) )/A20_KERNEL_BUILD
        ENTER_VERSION
        clear
        SELECT_CONFIG
        clear
        COMMON_STEPS
        ;;
            [nN][oO]|[nN])
        read -r -p "Starting build in existing directory? [Y/n] " input
        case $input in
            [yY][eE][sS]|[yY])
        ENTER_VERSION
        clear
        SELECT_CONFIG
        clear
        COMMON_STEPS
        ;;
            [nN][oO]|[nN])
        echo "Exit build process"
        exit 1
            ;;
            *)
        echo "Invalid input..."
        exit 1
        ;;
        esac
            ;;
            *)
        echo "Invalid input..."
        exit 1
        ;;
        esac
    fi
	sleep 2	
}

BUILD_KERNEL()
{	
	echo "*****************************************************"
	echo "        Building kernel for Samsung Galaxy A20       "
	echo "*****************************************************"

	export LOCALVERSION=_$VERSION
	if [ -d "$(dirname $(dirname $(realpath $0)) )/A20_KERNEL_BUILD" ]; then
        echo "Build directory exists, remove it first: $(dirname $(dirname $(realpath $0)) )/A20_KERNEL_BUILD"
        exit
    else
        echo "Creating build directory..."
        mkdir "$(dirname $(dirname $(realpath $0)) )/A20_KERNEL_BUILD"
        BUILD_DIR="$(dirname $(dirname $(realpath $0)) )/A20_KERNEL_BUILD"
        sleep 2
    fi
    
	make O=$BUILD_DIR $DEFCONFIG
    read -r -p "Start xconfig? [Y/n] " input
    case $input in
        [yY][eE][sS]|[yY])
    make xconfig O=$BUILD_DIR
    ;;
        [nN][oO]|[nN])
		echo "Continue build..."
    	;;
        *)
    echo "Invalid input..."
    exit 1
    ;;
    esac
	make -j$CORES O=$BUILD_DIR
	sleep 1	
}

ZIPPIFY()
{
	# Make Eureka flashable zip
	
	if [ -e "$BUILD_DIR/arch/$ARCH/boot/Image" ]
	then
	{
		echo -e "*****************************************************"
		echo -e "                                                     "
		echo -e "          Building anykernel flashable zip           "
		echo -e "                                                     "
		echo -e "*****************************************************"
		
		cp -rf $KERNEL_DIR/flashZip $BUILD_DIR/flashZip
		
		# Copy Image and dtbo.img to anykernel directory
		cp -f $BUILD_DIR/arch/$ARCH/boot/Image $BUILD_DIR/flashZip/anykernel/Image
#		cp -f $BUILD_DIR/arch/$ARCH/boot/dtbo.img $BUILD_DIR/flashZip/anykernel/dtbo.img
		
		# Go to anykernel directory
		cd $BUILD_DIR/flashZip/anykernel
		zip -r9 $ZIPNAME * -x .git README.md *placeholder
#		zip -r9 $ZIPNAME META-INF modules patch ramdisk tools anykernel.sh Image dtbo.img version
		chmod 0777 $ZIPNAME
		# Change back into kernel source directory
		cd $KERNEL_DIR
		sleep 1
	}
	fi
}

ENTER_VERSION()
{
	# Enter kernel revision for this build.
	read -p "Please type kernel version : " rev;
	if [ "${rev}" == "" ]; then
		echo " "
		echo "     Using '$REV' as version"
	else
		REV=$rev
		echo " "
		echo "     Version = $REV"
	fi
	sleep 2
}

RENAME()
{
	# Give proper name to kernel and zip name
	VERSION="v"$REV
	ZIPNAME=$ZIPNAME"_"$REV".zip"
}

DISPLAY_ELAPSED_TIME()
{
	# Find out how much time build has taken
	BUILD_END=$(date +"%s")
	DIFF=$(($BUILD_END - $BUILD_START))

	BUILD_SUCCESS=$?
	if [ $BUILD_SUCCESS != 0 ]
		then
			echo " Error: Build failed in $(($DIFF / 60)) minute(s) and $(($DIFF % 60)) seconds $reset"
			exit
	fi
	
	echo -e "                     Build completed in $(($DIFF / 60)) minute(s) and $(($DIFF % 60)) seconds $reset"
	sleep 1
}

SELECT_CONFIG()
{
	# setup selinux for differents firmwares
	echo -e "***************************************************************";
	echo "             Select which config you wish to use                  ";
	echo -e "***************************************************************";
	echo "Available versions:";
	echo " "
	echo "  1. OneUI";
	echo " "
	echo "  2. PostmarketOS";
	echo " "
	echo "Leave empty to exit this script";
	echo " "
	echo " "
	read -n 1 -p "Select your choice: " -s choice;
	case ${choice} in
		1)
		   {
			DEFCONFIG=oneui-a20_defconfig
			ZIPNAME=OneUI
		   };;
		2)
		   {
		   	DEFCONFIG=pmos-a20_defconfig
		   	ZIPNAME=PMOS
		   };;
		*)
		   {
			echo
			echo "Invalid choice entered. Exiting..."
			sleep 2
			exit 1
		   };;
	esac
}

COMMON_STEPS()
{
	echo "*****************************************************"
	echo "                                                     "
	echo "        Starting compilation of $DEVICE_Axxx kernel  "
	echo "                                                     "
	echo " Defconfig = $DEFCONFIG                              "
	echo "                                                     "
	echo "*****************************************************"
	RENAME
	sleep 1
	echo " "	
	BUILD_KERNEL
	echo " "
	sleep 1
	ZIPPIFY
	sleep 1
	echo " "
	DISPLAY_ELAPSED_TIME
	echo " "
	echo "                 *****************************************************"
	echo "*****************                                                     *****************"
	echo "                      build finished          "
	echo "*****************                                                     *****************"
	echo "                 *****************************************************"
	exit
}


#################################################################


###################### Script starts here #######################
CLEAN_SOURCE

